require('dotenv').config();

module.exports = {
  client: 'pg',
  connection: {
    host: process.env.PG_HOST,
    port: process.env.PG_PORT,
    database: process.env.PG_DATABASE,
    user: process.env.PG_USER,
    password: process.env.PG_PASSWORD,
  },
  pool: {
    min: 2,
    max: 6,
    createTimeoutMillis: 3000,
    acquireTimeoutMillis: 30000,
    idleTimeoutMillis: 30000,
    reapIntervalMillis: 1000,
    createRetryIntervalMillis: 100,
    propagateCreateError: false,
    afterCreate: (conn, cb) => {
      conn.query("SET timezone = 'UTC'", (err) => {
        cb(err, conn);
      });
    },
  },
  migrations: {
    tableName: 'knex_migrations',
  },
};
