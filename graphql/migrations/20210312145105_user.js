const defaultColumn = require('./functions/defaultColumns')

exports.up = function (knex) {
  return knex.schema.createTable('user', (table) => {
    table.increments('id').primary();
    table.unique(['username']);
    table.string('name').notNull();
    table.string('username').notNull();
    table.string('email').unique();
    table.string('phone').unique();
    table.string('password').notNull();
    table.string('old_password');
    table.boolean('first_access').default(true);
    table.boolean('is_admin').default(false);
    defaultColumn(knex, table)
  })
};

exports.down = function (knex) {
  return knex.schema.dropTable('user');
};
