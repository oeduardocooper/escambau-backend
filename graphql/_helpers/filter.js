module.exports = {
  filters(filter) {
    const filterBy = {}
    const tables = Object.keys(filter)
    for (const table of tables) {
      const columns = Object.keys(filter[`${table}`])
      for (const column of columns) {
        if (!Array.isArray(filter[`${table}`][`${column}`])) {
          filterBy[`${table}.${column}`] = filter[`${table}`][`${column}`]
        }
      }
    }
    return filterBy || {}
  },
  filtersWhereInTable(filter) {
    const tables = Object.keys(filter)
    for (const table of tables) {
      const columns = Object.keys(filter[`${table}`])
      for (const column of columns) {
        if (Array.isArray(filter[`${table}`][`${column}`])) {
          return `${table}.${column}`
        }
      }
    }
    return {}
  },
  filtersWhereInValue(filter) {
    const tables = Object.keys(filter)
    for (const table of tables) {
      const columns = Object.keys(filter[`${table}`])
      for (const column of columns) {
        if (Array.isArray(filter[`${table}`][`${column}`])) {
          if (filter[`${table}`][`${column}`].length > 0) {
            return filter[`${table}`][`${column}`]
          }
          return []
        }
      }
    }
    return []
  },
}
