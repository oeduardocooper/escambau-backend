module.exports = {
  s3: {
    credentials: {
      accessKeyId: process.env.S3_ACCESS_KEY_ID,
      secretAccessKey: process.env.S3_SECRET_ACCESS_KEY,
    },
    region: process.env.AWS_REGION,
    params: {
      ACL: 'public-read',
      Bucket: process.env.S3_BUCKET,
    },
  },
}
