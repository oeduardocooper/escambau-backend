const { createTestClient } = require('apollo-server-testing')
const { ApolloServer, gql } = require('apollo-server')
const { importSchema } = require('graphql-import')

const schemaPath = './schema/index.graphql'
const resolvers = require('../resolvers')


// cria um servidor de teste
const server = new ApolloServer({
  typeDefs: importSchema(schemaPath),
  resolvers,
  context: () => ({ user: { id: 1, email: 'a@a.a' } }),
});

it('authentication service', async () => {
  const client = createTestClient(server);
  const query = 'query auth($data: AuthInput!){ auth(data: $data){token}}'
  const res = await client.query({ query, variables: { data: { email: 'dev@escambau.com.br', password: '123123' } } })
  expect(res.data).toEqual({ getUser: 1 });
});
