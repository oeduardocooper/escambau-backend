const CryptoJS = require('crypto-js');

exports.seed = function (knex) {
  return knex('user').del()
    .then(() =>
      knex('user').insert([
        {
          name: 'Dev Escambau',
          username: 'escambau',
          email: 'dev@escambau.com.br',
          password: CryptoJS.AES.encrypt('123123', process.env.SECRET_PASS_KEY).toString(),
          phone: '5541998030915',
          first_access: true,
          is_admin: true,
          status: 1,
        },
      ])
    )
};
