const config = require('../knexfile.js')
const knex = require('knex')(config)
const moment = require('moment')

module.exports = {

  async authUserByUsername(username) {
    return knex.select(
      'user.id',
      'user.name',
      'user.username',
      'user.email',
      'user.phone',
      'user.password',
      'user.first_access',
      'user.status',
      'user.created_by',
      'user.updated_by',
      'user.created_at',
      'user.updated_at',
      'user.excluded',
    ).from('user')
      .first()
      .where({ 'user.username': username })
  },

  async getUserToRecoverPwd(username, id = null) { // remove
    return knex('user')
      .select('user.*')
      .first()
      .modify((queryBuilder) => {
        if (id) {
          queryBuilder.where('user.id', id)
        }
        if (username) {
          queryBuilder.where('user.username', username)
        }
      })
  },

  async getUsers(search, perPage, currentPage, pagination = false, order = null) {
    // ordenação padrão, caso não seja informada
    if (!order) {
      order = { column: 'user.name', order: 'asc' }
    }

    // a paginação não é obrigatória
    if (!perPage || !currentPage) {
      perPage = 9999
      currentPage = 1
    }
    const res = await knex('user')
      .select(
        'user.*',
      )
      .modify((queryBuilder) => {
        if (search) {
          queryBuilder
            .andWhere((builder) => builder
              .where('user.name', 'like', `%${search}%`)
              .orWhere('user.username', 'like', `%${search}%`)
              .orWhere('user.email', 'like', `%${search}%`)
              .orWhere('user.phone', 'like', `%${search}%`))
        }
      })
      .where({ 'user.excluded': false })
      .groupBy('user.id')
      .orderBy(order.column, order.order)
      .paginate({ perPage, currentPage })

    if (pagination) {
      return res
    }
    return res.data
  },

  getUser(id) {
    try {
      return knex('user')
        .select(
          'user.*',
        )
        .first()
        .where({ 'user.id': id })
    } catch (err) {
      throw new Error(err)
    }
  },

  async userCreate(data, ctx) {
    const { data: { id: created_by } } = ctx.validateUser();

    const params = {
      ...data,
      created_by,
    }
    return knex('user')
      .returning('*')
      .insert(params)
      .then(async (response) => response)
      .catch((err) => {
        throw err;
      })
  },

  async userUpdate(id, data, ctx) {
    try {
      const { data: { id: updated_by } } = ctx.validateUser();

      const params = {
        ...data,
        updated_by,
        updated_at: moment.utc().format(),
      }

      return knex('user')
        .where({ id })
        .update(params, '*')
    } catch (err) {
      throw new Error(err)
    }
  },

  async internalUserUpdate(id, data) {
    try {
      const params = {
        ...data,
        updated_at: moment.utc().format(),
      }
      return knex('user')
        .where({ id })
        .update(params, '*')
    } catch (err) {
      throw new Error(err)
    }
  },

  userDelete(id) {
    try {
      return knex('user')
        .where({ id })
        .del()
    } catch (err) {
      throw new Error(err)
    }
  },

}
