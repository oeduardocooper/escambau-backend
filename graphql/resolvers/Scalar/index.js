const { GraphQLDateTime } = require('graphql-iso-date');
const { GraphQLUpload } = require('graphql-upload')
const GraphQLJSON = require('graphql-type-json');
const DateFormatted = require('./scalarDateFormatted');

module.exports = {
  JSON: GraphQLJSON,
  Date: GraphQLDateTime,
  DateFormatted,
  UploadType: GraphQLUpload,
}
