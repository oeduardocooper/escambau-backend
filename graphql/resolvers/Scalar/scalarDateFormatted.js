const { GraphQLScalarType } = require('graphql');
const moment = require('moment')

// formata a data em YYYY-MM-DD HH:mm:ss
const DateFormatted = new GraphQLScalarType({
  name: 'DateFormatted',
  description: 'Date Formatted Scalar',
  serialize(value) {
    try {
      if (!moment(value).isValid()) {
        return null
      }
      return moment(value).format('YYYY-MM-DD HH:mm:ss')
    } catch (error) {
      return error
    }
  },
});

module.exports = DateFormatted;
