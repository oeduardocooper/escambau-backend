const CryptoJS = require('crypto-js');
const userModule = require('../../modules/User')
const { renewToken } = require('../Query/auth')
const { pubsub } = require('../../_helpers/pubsub')
const { getErrorItem } = require('../../_helpers/errorList')

module.exports = {
  async userCreate(_, args, ctx) {
    // verifica as permissões para acessar esta funcionalidade
    ctx && ctx.validateUser('can_create_user')

    const { data } = args;
    data.password = CryptoJS.AES.encrypt(data.password, process.env.SECRET_PASS_KEY).toString()
    if (data.username) {
      data.username = data.username.replace(/ /g, '')
    }

    if (!data.phone && Object.entries(data.email).length === 0) delete data.email
    if (!data.phone || Object.entries(data.phone).length === 0) delete data.phone

    if (data.email) {
      data.email = data.email.toLowerCase().replace(/ /g, '')
    }

    const res = await userModule.userCreate(data, ctx);
    await pubsub.publish('USER_CREATED', { userAdded: res[0] })
    return res[0] || false;
  },

  async userDelete(_, args, ctx) {
    // verifica as permissões para acessar esta funcionalidade
    ctx && ctx.validateUser('can_delete_user')
    const { id, force } = args;
    try {
      const user = await userModule.getUser(id)

      if (force) {
        await pubsub.publish('USER_DELETED', { userDeleted: user })
        return await userModule.userDelete(id, ctx)
      }

      const data = {
        excluded: true,
        username: `${user.username}-${id}-DELETED`,
        email: `${user.email}-${id}-DELETED`,
      }

      if (user.phone) {
        data.phone = `${user.phone}-${id}-DELETED`
      }

      await pubsub.publish('USER_DELETED', { userDeleted: user })

      return await userModule.userUpdate(id, data, ctx)
    } catch (err) {
      return err
    }
  },

  async userUpdate(_, args, ctx) {
    // verifica as permissões para acessar esta funcionalidade
    ctx && ctx.validateUser('can_update_user')

    const { id, data } = args

    if (data.password) {
      data.password = CryptoJS.AES.encrypt(data.password, process.env.SECRET_PASS_KEY).toString();
    }

    if (data.username) {
      data.username = data.username.replace(/ /g, '')
    }

    if (data.email) {
      data.email = data.email.toLowerCase().replace(/ /g, '')
    }

    const user = await userModule.userUpdate(id, data, ctx)
    return user[0]
  },

  async passwordChange(source, args, ctx) {
    ctx && ctx.validateUser()
    try {
      const { data: { id } } = ctx.validateUser()
      const { data: { password } } = args

      const user = await userModule.getUser(id)

      if (!user) {
        throw new Error(getErrorItem('user_not_found'))
      }

      // criptografa a senha uuid v4
      const newCrytedPassord = CryptoJS.AES.encrypt(
        password,
        process.env.SECRET_PASS_KEY,
      ).toString()

      // atualiza a senha do usuário
      const userData = {
        password: newCrytedPassord,
        first_access: false,
      }

      await userModule.userUpdate(id, userData, ctx)
      return renewToken(source, args, ctx)
    } catch (err) {
      return err
    }
  },
}
