const Query = require('./Query');
const Mutation = require('./Mutation');
const { JSON, DateFormatted } = require('./Scalar');

module.exports = {
  Query,
  Mutation,
  JSON,
  DateFormatted,
};
