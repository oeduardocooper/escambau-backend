const userModule = require('../../modules/User')
const { getErrorItem } = require('../../_helpers/errorList')

module.exports = {
  async getUsers(parent, args, ctx) {
    try {
      // verifica as permissões para acessar esta funcionalidade
      ctx && ctx.validateUser('can_view_user')

      const { search, per_page, page, order } = args
      return await userModule.getUsers(search, per_page, page, false, order)
    } catch (err) {
      return err
    }
  },

  async getUser(_, args, ctx) {
    // verifica as permissões para acessar esta funcionalidade
    ctx && ctx.validateUser('can_view_user')

    try {
      const { id } = args
      const user = await userModule.getUser(id)
      if (!user) {
        throw new Error(getErrorItem('user_not_found'))
      }
      return user
    } catch (err) {
      return err
    }
  },
}
