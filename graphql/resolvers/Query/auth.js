const jwt = require('jsonwebtoken')
const CryptoJS = require('crypto-js')
const userModule = require('../../modules/User')

const Messenger = require('../../_helpers/messenger')
const { getErrorList, getErrorItem } = require('../../_helpers/errorList')

async function generateToken(user) {
  const data = {
    id: user.id,
    name: user.name,
    username: user.username,
    email: user.email,
    status: user.status,
    first_access: user.first_access,
  }

  return {
    token: jwt.sign({
      data,
    }, process.env.APP_AUTH_SECRET),
  }
}

module.exports = {
  async auth(_, args) {
    const { data: { username, password } } = args

    const user = await userModule.authUserByUsername(username)

    if (!user) throw new Error(getErrorItem('user_not_found'))

    const decryptPassworld = CryptoJS.AES.decrypt(
      user.password,
      process.env.SECRET_PASS_KEY,
    ).toString(CryptoJS.enc.Utf8)

    if (decryptPassworld !== password) throw new Error(getErrorItem('incorrect_password'))

    return generateToken(user)
  },

  async renewToken(source, args, ctx) {
    ctx && ctx.validateUser()
    const { data: { id: userId } } = ctx.validateUser()

    const user = await userModule.getUser(userId)
    if (!user) {
      throw new Error(getErrorItem('user_not_found'))
    }
    return generateToken(user)
  },

  async recoverPassword(source, args) {
    try {
      const { data: { username } } = args

      if (!username) {
        throw new Error(getErrorItem('pin_and_username'))
      }

      const usernameClean = username ? username.replace(/ /g, '') : null

      const user = await userModule.getUserToRecoverPwd(usernameClean)

      if (!user) {
        throw new Error(getErrorItem('user_not_found'))
      }

      // gera nova senha
      const newPassord = Math.random().toString(36).substring(2, 5) + Math.random().toString(36).substring(2, 5);

      // criptografa a senha
      const newCrytedPassord = CryptoJS.AES.encrypt(
        newPassord,
        process.env.SECRET_PASS_KEY,
      ).toString()

      // atualiza a senha do usuário
      const userData = {
        password: newCrytedPassord,
        // old_password: user.password,
        first_access: true,
      }
      await userModule.internalUserUpdate(user.id, userData)

      const body = `
        Olá, ${user.name}. <br>
        Sua senha temporária é: <strong>${newPassord}</strong>
      `;

      const resultset = [];
      // envia o email
      const messenger = new Messenger()
      if (user.email) {
        const result = await messenger.sendEmail(
          null,
          [user.email],
          body,
          'Recuperação de senha (não responda)',
        )
        resultset.push({
          status: result !== '',
          message: (result !== '') ? 'e-mail Password Recovery success' : 'e-mail Password Recovery failure',
        })
      }

      // envio de sms - está desativado por enquanto
      if (user.phone && user.phone.length === 13) {
        // enviar sms - o telefone tem que ter a identificação do país
        const result = await messenger.sendSms(user.phone, `Sua senha temporária é: ${newPassord}`)
        if (result !== false) {
          resultset.push({
            status: result !== '',
            message: (result !== '') ? 'SMS Password Recovery success' : 'SMS Password Recovery failure',
          })
        }
      }
      return resultset
    } catch (error) {
      throw new Error(error)
    }
  },
  async errorList(source, args, ctx) {
    try {
      ctx && ctx.validateUser()
      return getErrorList()
    } catch (error) {
      throw new Error(error)
    }
  },
}
