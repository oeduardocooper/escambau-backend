const { pubsub } = require('../../_helpers/pubsub')

module.exports = {
  userAdded: {
    // subscriber para criação de usuário
    subscribe: () => pubsub.asyncIterator('USER_CREATED'),
  },
  userDeleted: {
    // subscriber para deleção de usuário
    subscribe: () => pubsub.asyncIterator('USER_DELETED'),
  },
}
