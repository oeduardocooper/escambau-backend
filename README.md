# Bem vindo à API do projeto Escambau!

Este projeto de api tem como intuito fazer com que pequenos produtores possam oferecer seus produtos e possibilitar a troca dos mesmos com outros produtores

# A API

Desenvolvida utilizando **Apollo Server** - framework GraphQL, que roda junto com o Express utilizando o módulo **apollo-server-express**, o arquivo de configuração principal é o **server.js**
- A API roda em uma máquina **docker**
- O banco de dados também fica em uma máquina **docker**
- As variáveis de ambiente encontram-se divididas em arquivos distintos para desenvolvimento, teste e produção
- O ORM utilizado é o **Knex**. Responsável por gerenciar a estrutura do banco de dados, alimentar as tabelas inicialmente. Nada deve ser criado diretamente no banco de dados, qualquer alteração deve ser feita via mutation.


## Estrutura de arquivos e pastas
>  **_helpers**
> Estruturas de código que podem ser utilizadas em vários pontos do projeto

> **migrations**
> Os arquivos aqui contidos são gerados automaticamente, um migration é criado pelo seguinte comando:$ npx knex migrate:make nomedatabela_table

> **modules**
> São os arquivos responsáveis por "conversar" com o banco de dados. Nenhuma requisição ao banco de dados deve ser realizada fora deste local

## Executando a aplicação

### para o projeto rodar localmente:

$ yarn install
>**_executar dentro da pasta graphql_**

$ make dev (na raiz do projeto)
>**_executa e roda o projeto localmente_**

$ make migrate (na raiz do projeto)
>**Cria a base de dados**

$ make logs escambau_graphql
>**_exibe os logs em tempo de execução_**

### outros comandos:

$ make build (na raiz do projeto)
>**_Executa o build do projeto_**

$ make up (na raiz do projeto)
 >**_Executa o projeto_**

$ make stop (na raiz do projeto)
>**_Pára o projeto_**

$ make reset (na raiz do projeto)
>**Reseta a base de dados**

### GraphQL Apollo Server
> **url:** http://localhost:4000/graphql
> **consultar os payloads no fim deste documento**

### Postgres Admin
> **url:** http://localhost:16543/login
> **login:** dev@escambau.com.br
> **password:** escambau
>

 **_configurações do painel do Postgres Admin:_**
 > **host:** escambau_postgres
> **database:** postgres
>**username:** escambau
>**password:** escambau

## Payloads do Graphql
Lista de payloads para a execução de querys e mutations no GraphQL Playground (http://localhost:4000/)

> auth
{
  auth(
    data: {
      username: "escambau"
      password: "123123"
    }){
      token
    }
}

> getUser
{
  getUser(id: 2){
    id
    name
    username
    email
 	}
}

> getUsers
{
  getUsers(search: "dev") {
    id
    name
  }
}

> renewToken
{
  renewToken{
    token
  }
}

> errorList
{
  errorList
}

> userCreate
mutation{
  userCreate(data: {
    name: "teste"
    username: "teste"
    email: "teste2@teste.com"
    phone: "554198987898"
    password: "123123"
    status: 1
  }){
    id
    name
    username
  }
}

> userUpdate
mutation{
  userUpdate(id: 3 data:{
    name: "teste um"
    email: "testeum@teste.com.br"
    phone: "5541999999999"
  }){
    name
    email
    phone
  }
}

> userDelete
mutation{
  userDelete(id: 3)
}

mutation{
  userDelete(id: 3, force: true)
}

> passwordChange
mutation{
  passwordChange(data:{password:"123123"}){
    token
  }
}

> recoverPassword
{
  recoverPassword(data:{username:"escambau"}){
    status
    message
  }
}